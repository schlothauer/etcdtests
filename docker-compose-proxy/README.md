A simple docker-compose based etcd cluster.

To avoid dependencies to local ip addresses it needs a declared env variable
MY_LOCAL_IP that points to the ip address of local machine 

To start the cluster use the start script in bin directory. It can start or 
create the cluster and take care for the needed directories.

```
MY_LOCAL_MACHINE=192.168.0.99 ./bin/start.sh
```

```bash
# query current users
curl http://127.0.0.1/etcd3/v2/auth/users

# add a root user
curl http://127.0.0.1/etcd3/v2/auth/users/root -XPUT -d '{"user": "root","password": "IchBinRoot"}'

# add authentication
curl http://127.0.0.1/etcd3/v2/auth/enable -XPUT -d 'true'

# add extra users
curl http://127.0.0.1/etcd3/v2/auth/users/anna -u root:IchBinRoot -XPUT -u root:IchBinRoot -d '{"user": "anna","password": "IchBinAnna"}'
curl http://127.0.0.1/etcd3/v2/auth/users/vera -u root:IchBinRoot -XPUT -d '{"user": "vera","password": "IchBinVera"}'
curl http://127.0.0.1/etcd3/v2/auth/users/ines -u root:IchBinRoot -XPUT -d '{"user": "ines","password": "IchBinInes"}'

# should fail
#curl http://127.0.0.1/etcd3/v2/auth/users/test -u ines:IchBinInes -XPUT -d '{"user": "test","password": "IchBinTest"}'

# modify the grants for the normal users

curl http://127.0.0.1/etcd1/v2/keys/vera/status/lastPing -XPUT -d value="Mi 19. Apr 16:38:16 CEST 2017"

```

![Deployment structure](documentation/etcd_proxy.png)
