#!/bin/bash

# start or setup the keycloak/apache/tomcat environment 

scriptPos=${0%/*}

absPathToBase=$(pushd $scriptPos/.. > /dev/null; pwd ; popd > /dev/null)


composeFile="$scriptPos/../docker-compose.yml"

if ! [ -d $scriptPos/../cont1/data ]; then mkdir -p $scriptPos/../cont1/data; fi
if ! [ -d $scriptPos/../cont2/data ]; then mkdir -p $scriptPos/../cont2/data; fi
if ! [ -d $scriptPos/../cont3/data ]; then mkdir -p $scriptPos/../cont3/data; fi


# create the certificate for the apache webserver
if ! [ -f $scriptPos/../httpd/data/ssl/apache.pem ]; then
    pushd $scriptPos/../httpd/data/ssl > /dev/null
    openssl req -config cert_config.txt -new -x509 -days 730 -nodes -out ./apache.pem -keyout ./apache.pem
    ln -sf ./apache.pem ./`/usr/bin/openssl x509 -noout -hash < ./apache.pem`.0
    chmod 600 ./apache.pem
    popd > /dev/null
fi


contName1=etcd-tests-proxy-c1

docker ps -f name="$contName1" | grep "$contName1" > /dev/null && echo -en "\033[1;31m  container seems to be up: $contName1\033[0m\n" && exit 1


if docker ps -a -f name="$contName1" | grep "$contName1" > /dev/null; then
    docker-compose -f "$composeFile" start
else
    docker-compose -f "$composeFile" up -d
fi

