A simple docker-compose based etcd cluster.

To avoid dependencies to local ip addresses it needs a dns declared name
'etcd-tests' that points to ip address of local machine.

The configuration can be done in '/etc/hosts'

```
# /etc/hosts

192.168.178.26 etcd-hosts
```

To start the cluster use the start script in bin directory. It can start or 
create the cluster and take care for the needed directories.

```
./bin/start.sh
```
