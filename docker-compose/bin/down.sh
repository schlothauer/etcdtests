#!/bin/bash

# stop the test environment

scriptPos=${0%/*}

composeFile="$scriptPos/../docker-compose.yml"

docker-compose -f "$composeFile" down

sudo rm -rf $scriptPos/../cont1
sudo rm -rf $scriptPos/../cont2
sudo rm -rf $scriptPos/../cont3
